
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.ImageIcon;

public class Tiro {

    private static final int ALTURA_TIRO = 20;
    private static final int LARGURA_TIRO = 20;
    private static final int DESLOCAMENTO = 20;

    private static Image imagem = null;

    private int x = 0;
    private int y = 0;

    private boolean disparada = true;
    private boolean contato = false;

    private int current_slide = 0;
    private ArrayList<Image> tiro = new ArrayList<>();

    public Tiro(Personagem origem) {
        /* como "imagem" é estática, só precisa fazer uma vez se ela não tiver sido inicializada */
        if (imagem == null) {
            //carga do tiro
            for (int i = 1; i <= 7; i++) {
                Image cargaTiro = Toolkit.getDefaultToolkit().getImage("src/sprites/tiro" + i + ".gif");
                ImageIcon iconeTiro = new ImageIcon(cargaTiro);
                tiro.add(iconeTiro.getImage());
            }
        }

        //aparece à direita da origem, em 2/3 da sua altura
        this.x = origem.getX() + origem.getLargura();
        this.y = origem.getY() + (origem.getAltura()/10 -2);
    }

    public Image getImage() {
        if(this.contato == true)
            this.current_slide += 1;
//            System.out.println(this.current_slide);
        if( this.current_slide == 6 ){
            this.contato = false;
            this.disparada = false;
            return null;
        }
        if(this.contato == false)
            return this.tiro.get(0);
        else
            return this.tiro.get(this.current_slide);
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getAltura() {
        return ALTURA_TIRO;
    }

    public int getLargura() {
        return LARGURA_TIRO;
    }

    public int getDeslocamento() {
        return DESLOCAMENTO;
    }

    public void sumir() {
//        this.disparada = false;
        this.contato = true;
    }

    public void mover() {
        this.x += DESLOCAMENTO;
    }

    public boolean isDisparada() {
        return this.disparada;
    }

    public boolean isContato() {
        return this.contato;
    }

}
