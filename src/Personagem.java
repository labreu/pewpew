
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Personagem {
    
        
    private static final int ALTURA_PERSONAGEM = 150;
    private static final int LARGURA_PERSONAGEM = 150;
    
    private static final int POSICAO_INICIAL_X_DO_PERSONAGEM = 0;
    private static final int POSICAO_Y_DO_PERSONAGEM = 350;
    
    private static final int MOVIMENTO_DO_PERSONAGEM = 5;
    
    private Image imagem = null;
    private int x = POSICAO_INICIAL_X_DO_PERSONAGEM;
    private int y = POSICAO_Y_DO_PERSONAGEM;
    private boolean andandoParaDireita = false;
    private boolean andandoParaEsquerda = false;
    private boolean andandoParaCima = false;
    private boolean andandoParaBaixo = false;
    private boolean viradoParaDireita = true;
    private boolean viradoParaEsquerda = false;
    private boolean atirando = false;
    
    private int current_slide = 0;
    private ArrayList<Image> personagem = new ArrayList<>();
    
    public Personagem(){
        
        // Carrega na lista 0-9 imagens de correndo para direita
        for( int i = 1; i <= 10; i++ ){
            Image cargaPersonagem = Toolkit.getDefaultToolkit().getImage("src/sprites/runR" + i + ".gif");
//            cargaPersonagem = cargaPersonagem.getScaledInstance(LARGURA_PERSONAGEM, ALTURA_PERSONAGEM, Image.SCALE_DEFAULT);
            ImageIcon iconePersonagem = new ImageIcon(cargaPersonagem);
            personagem.add(iconePersonagem.getImage());
        }

        // Carrega na lista 10-19 imagens de correndo para esquerda
        for( int i = 1; i <= 10; i++ ){
            Image cargaPersonagem = Toolkit.getDefaultToolkit().getImage("src/sprites/runL" + i + ".gif");
//            cargaPersonagem = cargaPersonagem.getScaledInstance(LARGURA_PERSONAGEM, ALTURA_PERSONAGEM, Image.SCALE_DEFAULT);
            
            ImageIcon iconePersonagem = new ImageIcon(cargaPersonagem);
            personagem.add(iconePersonagem.getImage());
        }

        // Carrega na lista 20-29 imagens de atirando correndo para direita
        for( int i = 1; i <= 10; i++ ){
//            Image cargaPersonagem = Toolkit.getDefaultToolkit().getImage("images/runFireR" + i + ".gif");
            Image cargaPersonagem = Toolkit.getDefaultToolkit().getImage("src/sprites/runR" + i + ".gif");
//            cargaPersonagem = cargaPersonagem.getScaledInstance(LARGURA_PERSONAGEM, ALTURA_PERSONAGEM, Image.SCALE_DEFAULT);
            ImageIcon iconePersonagem = new ImageIcon(cargaPersonagem);
            personagem.add(iconePersonagem.getImage());
        }
        
        // Carrega na lista 30-39 imagens de atirando correndo para esquerda
        for( int i = 1; i <= 10; i++ ){
//            Image cargaPersonagem = Toolkit.getDefaultToolkit().getImage("images/runFireL" + i + ".gif");
            Image cargaPersonagem = Toolkit.getDefaultToolkit().getImage("src/sprites/runL" + i + ".gif");
//            cargaPersonagem = cargaPersonagem.getScaledInstance(LARGURA_PERSONAGEM, ALTURA_PERSONAGEM, Image.SCALE_DEFAULT);
            ImageIcon iconePersonagem = new ImageIcon(cargaPersonagem);
            personagem.add(iconePersonagem.getImage());
        }
        // Carrega na lista 40-44 imagens parado para esquerda
        for( int i = 1; i <= 5; i++ ){
//            Image cargaPersonagem = Toolkit.getDefaultToolkit().getImage("images/runFireL" + i + ".gif");
            Image cargaPersonagem = Toolkit.getDefaultToolkit().getImage("src/sprites/standR" + i + ".gif");
//            cargaPersonagem = cargaPersonagem.getScaledInstance(LARGURA_PERSONAGEM, ALTURA_PERSONAGEM, Image.SCALE_DEFAULT);
            ImageIcon iconePersonagem = new ImageIcon(cargaPersonagem);
            personagem.add(iconePersonagem.getImage());
        }
        // Carrega na lista 45-50 imagens parado para direita
        for( int i = 1; i <= 6; i++ ){
//            Image cargaPersonagem = Toolkit.getDefaultToolkit().getImage("images/runFireL" + i + ".gif");
            Image cargaPersonagem = Toolkit.getDefaultToolkit().getImage("src/sprites/standL" + i + ".gif");
//            cargaPersonagem = cargaPersonagem.getScaledInstance(LARGURA_PERSONAGEM, ALTURA_PERSONAGEM, Image.SCALE_DEFAULT);
            ImageIcon iconePersonagem = new ImageIcon(cargaPersonagem);
            personagem.add(iconePersonagem.getImage());
        }
    }
    
    //uso da imagem do personagem
    public Image getImage(){
//        System.out.println(this.x);
        this.current_slide +=1;
        // de 3 em 3 'frames' dá uma sensação visual mais leve no movimento 
        if(this.current_slide > 27)
            this.current_slide = 0;
        if(this.isAndandoParaDireita()){
            this.viradoParaDireita = true;
            this.viradoParaEsquerda = false;
            return this.personagem.get(this.current_slide/3);
        }
//        if(this.isAndandoParaEsquerda())
        if(this.isAndandoParaEsquerda()){
            this.viradoParaDireita = false;
            this.viradoParaEsquerda = true;
            return this.personagem.get(this.current_slide/3 +10);
        }
        if( this.isViradoParaDireita())
            return this.personagem.get(this.current_slide/5%5 +40);
        else
            return this.personagem.get(this.current_slide/6%6 + 45);
        
    }
    
    public int getX(){
        return this.x;
    }
    
    public int getY(){
        return this.y;
    }
    
    public int getAltura(){
//        this.getImage().getHeight();
        return ALTURA_PERSONAGEM;
    }
    
    public int getLargura(){
        return LARGURA_PERSONAGEM;
    }
    
    public boolean isAndandoParaDireita(){
        return this.andandoParaDireita;
    }
    
    public boolean isAndandoParaEsquerda(){
        return this.andandoParaEsquerda;
    }
    
    public boolean isAndandoParaCima(){
        return this.andandoParaCima;
    }
    
    public boolean isAndandoParaBaixo(){
        return this.andandoParaBaixo;
    }
    
    public boolean isViradoParaDireita(){
        return this.viradoParaDireita;
    }
    
    public boolean isViradoParaEsquerda(){
        return this.viradoParaEsquerda;
    }
    
    public boolean isAtirando(){
        return this.atirando;
    }
    
    public void moverParaDireita(){
        this.x += MOVIMENTO_DO_PERSONAGEM;
    }
    
    public void moverParaEsquerda(){
        this.x -= MOVIMENTO_DO_PERSONAGEM;
    }
    
    public void moverParaCima(){
        this.y -= MOVIMENTO_DO_PERSONAGEM;
    }
    
    public void moverParaBaixo(){
        this.y += MOVIMENTO_DO_PERSONAGEM;
    }
    
    public void comecarAAndar(int direcao){
        switch(direcao){
            //vale lembrar que ESQUERDA e código da tecla esquerda do teclado são os mesmos
            case JanelaDoJogo.ESQUERDA:
                this.andandoParaEsquerda = true;
                break;
            //o mesmo de cima vale para DIREITA
            case JanelaDoJogo.DIREITA:
                this.andandoParaDireita = true;
                break;
            case JanelaDoJogo.CIMA:
                this.andandoParaCima = true;
                break;
            case JanelaDoJogo.BAIXO:
                this.andandoParaBaixo = true;
                break;
            default:
        }
    }
    
    public void pararDeAndar(int direcao){
        switch(direcao){
            case JanelaDoJogo.ESQUERDA:
                this.andandoParaEsquerda = false;
                break;
            case JanelaDoJogo.DIREITA:
                this.andandoParaDireita = false;
                break;
            case JanelaDoJogo.CIMA:
                this.andandoParaCima = false;
                break;
            case JanelaDoJogo.BAIXO:
                this.andandoParaBaixo = false;
                break;
            default:
        }
    }
    
}
